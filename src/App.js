import React, { useState, useRef, useEffect } from 'react';
import {v4 as uuidv4} from 'uuid';
import ToDoList from './ToDoList';
import './App.css';

const LOCAL_STORAGE_KEY = "todoApp.todos";

function App() {
    const [todos, setTodos] = useState([]);
    const toDoNameRef = useRef()

    useEffect(() => {
        const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
        if (storedTodos) setTodos(storedTodos);
    }, []);

    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos))
    }, [todos]);

    function toggleTodo(id) {
        const newTodos = [...todos];
        const todo = newTodos.find(todo => todo.id === id)
        todo.completed = !todo.completed;
        setTodos(newTodos);
    }

    function handleAddTodo() {
        const name = toDoNameRef.current.value;
        if (name === '') return;
        toDoNameRef.current.value = null;
        setTodos(previous => {
            return [...previous, { id: uuidv4(), name, completed: false }]
        });
    }

    function handleClearCompleted() {
        setTodos(todos.filter(row => !row.completed));;
    }

    return (
        <>
            <h1>To Do List</h1>
            <ToDoList todos={todos} toggleTodo={toggleTodo} />
            <input type="text" ref={toDoNameRef} role="inputName" />
            <button onClick={handleAddTodo} role="addButton">Add</button>
            <button onClick={handleClearCompleted} role="clearButton">Clear Completed</button>
            <div>{todos.length} ToDo's Remaining</div>
        </>
    );
}

export default App;

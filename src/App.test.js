import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

beforeEach(() => {
  render(<App />);
});

describe("interface", () => {
  it("displays input element", () => {
    expect(screen.getByRole("inputName")).toBeInTheDocument();
  });
  it("displays add button", () => {
    expect(screen.getByRole("addButton")).toBeInTheDocument();
  });
  it("displays clear button", () => {
    expect(screen.getByRole("clearButton")).toBeInTheDocument();
  });
});

describe("State", () => {
  it("can add todo", async () => {
    screen.getByRole("inputName").value = "TEST INPUT";
    screen.getByRole("addButton").click();
    expect(screen.getByText("TEST INPUT")).toBeInTheDocument();
  });
  it("can remove todo", () => {});
  it("can clear completed todos", () => {});
  it("displays correct number of results", () => {});
});

/*
TODO: include unit, component, integration tests
 */
